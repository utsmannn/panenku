### PANENKU

| Num  | Task / Feature                         | Component                                        | Progress | Start Date | End Date  | Testing Pass | Status | Note                                    | Reference |
| ---- | -------------------------------------- | ------------------------------------------------ | -------- | ---------- | --------- | ------------ | ------ | --------------------------------------- | --------- |
| 1    | Database / Preparing                   | Backendless                                      | 100%     | 12-Oct-19  | 17-Oct-19 | PASS         | DONE   |                                         |           |
| 2    | Database / Instance  Class             | Retrofit                                         | 100%     | 12-Oct-19  | 17-Oct-19 | PASS         | DONE   |                                         |           |
| 3    | UI                                     | XML                                              | 80%      | 14-Oct-19  |           |              |        |                                         |           |
| 4    | Feature / Dashboard                    | Fragment                                         | 90%      | 15-Oct-19  |           |              |        | For  data item type sample use hardcode |           |
| 5    | Feature / Database /  GetListData      | Retrofit,  DatabaseApp.java                      | 100%     | 16-Oct-19  | 17-Oct-19 | PASS         | DONE   |                                         |           |
| 6    | Feature / Database /  SaveObject       | Retrofit,  DatabaseApp.java                      | 90%      | 17-Oct-19  |           |              |        |                                         |           |
| 7    | Feature / Database /  GetListDataQuery | Retrofit,  DatabaseApp.java                      | 90%      | 17-Oct-19  |           |              |        |                                         |           |
| 8    | Feature / Database /  Auth             | Retrofit,  Auth Module, AuthApp.java, SharedPref | 100%     | 17-Oct-19  | 17-Oct-19 | PASS         | DONE   |                                         |           |
| 9    | Feature / Order /  Checkout            | Retrofit,  DatabaseApp.java                      | -        | -          | -         |              |        |                                         |           |
| 10   | Feature / Order /  Communication       | Retrofit,  DatabaseApp.java, MQTT                | -        | -          | -         |              |        |                                         |           |
| 11   | Testing / Behaviour                    | Behaviour                                        | -        | -          | -         |              |        |                                         |           |


