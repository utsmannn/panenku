package com.utsman.auth;

public interface UserLoginListener {
    void onLoginSuccess(User user);
    void onLoginUnauthorized();
    void onLoginFailed(Throwable throwable);
}
