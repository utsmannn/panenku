package com.utsman.auth

data class User(val name: String,
                val location: String,
                val email: String,
                val photoProfile: String,
                val password: String? = null,
                val objectId: String? = null)