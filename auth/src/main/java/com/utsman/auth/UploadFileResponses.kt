package com.utsman.auth

data class UploadFileResponses(val fileURL: String)