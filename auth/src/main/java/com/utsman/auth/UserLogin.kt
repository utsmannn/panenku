package com.utsman.auth

data class UserLogin(val login: String,
                     val password: String)