package com.utsman.auth;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthApp {
    private String appId;
    private String restKey;

    public AuthApp(String appId, String restKey) {
        this.appId = appId;
        this.restKey = restKey;
    }

    private AuthInstance instance = AuthInstance.Companion.create();

    public void register(User user, final UserLoginListener loginListener) {
        instance.register(appId, restKey, user)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        loginListener.onLoginSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        loginListener.onLoginFailed(t);
                    }
                });
    }

    public void login(UserLogin userLogin, final UserLoginListener loginListener) {
        instance.login(appId, restKey, userLogin)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.code() != 401) {
                            loginListener.onLoginSuccess(response.body());
                        } else  {
                            loginListener.onLoginUnauthorized();
                        }
                        Log.i("anjay", "onResponse: " + response.code());
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        loginListener.onLoginFailed(t);
                    }
                });
    }
}
